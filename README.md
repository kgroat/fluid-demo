# Fluid demo
## Based on [Fluid Simulation for Dummies](https://mikeash.com/pyblog/fluid-simulation-for-dummies.html) by [Mike Ash](https://mikeash.com/)

### [See the code in action](https://kg-fluid-demo.herokuapp.com/)

To start the app, run:
* `npm i` -- to install all of the dependencies
* `npm start` -- to start the webpack dev server

You can then open your browser to http://localhost:3000/ to see the demo in action
