
import webpack from 'webpack'

import path from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'


function createMainConfig (): webpack.Configuration & { devServer: { [k: string]: any } } {
  return {
    bail: false,
    mode: 'development',
    devtool: 'source-map',
    entry: [
      path.join(__dirname, 'src/index.ts'),
    ],
    devServer: {
      host: '0.0.0.0',
      port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
      contentBase: [
        path.join(__dirname, 'static'),
      ],
      compress: true,
      clientLogLevel: 'none',
      watchContentBase: true,
      hot: true,
      // TODO: remove this once this issue is resolved:
      // https://github.com/webpack/webpack-dev-server/issues/1604
      disableHostCheck: true,
    },
    output: {
      pathinfo: true,
      publicPath: '/',
      filename: 'static/js/[name].[hash:8].js',
      chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
      // Point sourcemap entries to original disk location (format as URL on Windows)
      devtoolModuleFilenameTemplate:
        (info: any) =>
          path
            .resolve(info.absoluteResourcePath)
            .replace(/\\/g, '/')
    },
    resolve: {
      extensions: [
        '.ts',
        '.js',
        '.json',
      ],
      plugins: [
        new TsconfigPathsPlugin({ configFile: path.join(__dirname, 'tsconfig.json') }),
      ],
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          oneOf: [
            {
              test: /\.ts$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              options: {
                compact: true,
                cacheDirectory: true,
                babelrc: false,
                presets: [
                  '@babel/preset-env',
                  '@babel/preset-typescript',
                ],
                plugins: [
                  ['@babel/plugin-proposal-decorators', { legacy: true }],
                  ['@babel/plugin-proposal-class-properties', { loose: true }],
                ],
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        inject: 'body',
        template: path.join(__dirname, 'static/index.html'),
      }),
    ],
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    optimization: {
      namedModules: true,
    },
  }
}

export default createMainConfig
