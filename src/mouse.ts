
import { canvas } from './canvas'
import { SCALE } from './constants'
import { store } from './store'

export let mouseDown = false
export let mouseX = 0
export let mouseY = 0

canvas.addEventListener('mousedown', (ev) => {
  mouseDown = true
  mouseX = ev.offsetX
  mouseY = ev.offsetY
})

document.addEventListener('mouseup', () => {
  mouseDown = false
})

canvas.addEventListener('mousemove', (ev) => {
  if (mouseDown) {
    moveMouse(ev.offsetX, ev.offsetY)
  }
})

canvas.addEventListener('touchstart', (ev) => {
  mouseDown = true
  const [x, y] = mapTouchPosition(ev)
  mouseX = x
  mouseY = y
})

canvas.addEventListener('touchend', (ev) => {
  mouseDown = false
})

canvas.addEventListener('touchmove', (ev) => {
  moveMouse(...mapTouchPosition(ev))
})

function moveMouse (x: number, y: number) {
  const { force } = store.getState()
  const dx = (x - mouseX) * force
  const dy = (y - mouseY) * force

  store.addVelocity(Math.floor(x / SCALE), Math.floor(y / SCALE), dx, dy)

  mouseX = x
  mouseY = y
}

function mapTouchPosition (ev: TouchEvent): [number, number] {
  const { top, left } = canvas.getBoundingClientRect()
  return [
    ev.touches[0].clientX - left,
    ev.touches[0].clientY - top,
  ]
}
