
export type State = Readonly<{
  timestep: number
  force: number
  diffusion: number
  viscosity: number
  dyeStrength: number
  diffuseIterations: number
  projectIterations: number
  s: number[]
  velocityZeroes: Readonly<[number[], number[]]>
  velocities: Readonly<[number[],number[]]>
  dye: number[]
  dyeColor: [number, number, number]
}>
