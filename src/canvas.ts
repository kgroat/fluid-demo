
import { idx } from './arrayHelpers'
import { CANVAS_SIZE, SCALE } from './constants'
import { store } from './store'

export const canvas = document.createElement('canvas')
canvas.width = CANVAS_SIZE
canvas.height = CANVAS_SIZE
canvas.style.width = `${CANVAS_SIZE * SCALE}px`
canvas.style.height = `${CANVAS_SIZE * SCALE}px`
canvas.style.cursor = 'crosshair'

export function render() {
  const { dye, dyeColor: [r, g, b] } = store.getState()
  const ctx = canvas.getContext('2d')!
  ctx.clearRect(0, 0, CANVAS_SIZE, CANVAS_SIZE)

  for (let x = 0; x < CANVAS_SIZE; x++) {
    for (let y = 0; y < CANVAS_SIZE; y++) {
      const i = idx(x + 1, y + 1)
      if (Number.isNaN(dye[i])) {
        ctx.fillStyle = `#0F0`
      } else {
        ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${Math.max(0, Math.min(dye[i], 1))})`
      }
      ctx.fillRect(x, y, 1, 1)
    }
  }
}
