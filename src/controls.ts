
import { store } from './store'

const { force, diffusion, viscosity, dyeStrength, dyeColor } = store.getState()

export const controls = document.createElement('div')
controls.id = 'controls'
controls.appendChild(buildLabel('Force: ', buildSlider('force', 0, 0, force, 0, 100, 1)))
controls.appendChild(buildLabel('Diffusion: ', buildSlider('diffusion', 4.5, 0.05, diffusion, 0, 1, 0.01)))
controls.appendChild(buildLabel('Viscosity: ', buildSlider('viscosity', 3.01, 0.1, viscosity, 0, 1, 0.01, 2)))
controls.appendChild(buildLabel('Dye strength: ', buildSlider('dyeStrength', 0, 0, dyeStrength, 0, 100, 1)))
controls.appendChild(buildLabel('Dye color: ', buildColorInput(dyeColor, 'dyeColor'), 'dyeColor'))

type SliderType = 'force' | 'diffusion' | 'viscosity' | 'dyeStrength'
function buildSlider (type: SliderType, offset: number, power: number, value = 0, min = 0, max = 10, step = 0.1, mult = 1) {
  const slider = document.createElement('input')
  slider.type = 'range'
  slider.value = `${value}`
  slider.min = `${min}`
  slider.max = `${max}`
  slider.step = `${step}`
  const text = document.createElement('input')
  text.type = 'number'
  text.value = `${value}`
  text.min = `${min}`
  text.max = `${max}`
  text.step = `${step}`

  slider.addEventListener('change', () => {
    if (text.value !== slider.value) {
      text.value = slider.value
    }
    const value = parseFloat(slider.value) * mult

    store.setState({
      [type]: power ? mapToTrueValue(value, power, offset + max) : value,
    })
  })

  text.addEventListener('change', () => {
    if (slider.value !== text.value) {
      slider.value = text.value
    }
    const value = parseFloat(text.value) * mult

    store.setState({
      [type]: power ? mapToTrueValue(value, power, offset + max) : value,
    })
  })

  return [slider, text]
}

function mapToTrueValue (value: number, power: number, offset: number) {
  return value === 0
    ? 0
    : Math.pow(power, (offset - value))
}

function buildLabel (text: string, inputs: (HTMLInputElement[] | HTMLInputElement), htmlFor?: string) {
  const label = document.createElement('label')
  label.appendChild(document.createTextNode(text))
  if (inputs instanceof Array) {
    inputs.forEach(i => label.appendChild(i))
  } else {
    label.appendChild(inputs)
  }

  if (htmlFor) {
    label.htmlFor = htmlFor
  }

  return label
}

function buildColorInput (color: [number, number, number], id: string) {
  let hex = toHex(color)
  const colorInput = document.createElement('input')
  colorInput.id = id
  colorInput.type = 'color'
  colorInput.value = hex
  const colorTextInput = document.createElement('input')
  colorTextInput.type = 'text'
  colorTextInput.value = hex

  colorInput.addEventListener('change', () => {
    hex = colorInput.value
    store.setState({
      dyeColor: parseHex(hex),
    })

    if (colorTextInput.value !== hex) {
      colorTextInput.value = hex
    }
  })

  colorTextInput.addEventListener('keyup', () => {
    if (!HEX_RGX.test(colorTextInput.value)) {
      return
    }
    hex = colorTextInput.value
    store.setState({
      dyeColor: parseHex(hex),
    })

    if (colorInput.value !== hex) {
      colorInput.value = hex
    }
  })

  return [colorInput, colorTextInput]
}

function hexPad (val: number) {
  let hex = val.toString(16)
  if (hex.length < 2) {
    hex = '0' + hex
  }

  return hex
}

function toHex ([r, g, b]: [number, number, number]) {
  return `#${hexPad(r)}${hexPad(g)}${hexPad(b)}`
}

const HEX_RGX = /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i
function parseHex (hex: string): [number, number, number] {
  const [, rh, gh, bh] = HEX_RGX.exec(hex)!
  return [parseInt(rh, 16),parseInt(gh, 16),parseInt(bh, 16)]
}
