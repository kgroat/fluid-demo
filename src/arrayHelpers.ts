
import { SIZE } from './constants'

export function idx (x: number, y: number) {
  x = Math.max(0, Math.min(x, SIZE - 1))
  y = Math.max(0, Math.min(y, SIZE - 1))
  return Math.round(x + y * SIZE)
}

export function ridx (idx: number) {
  return [idx % SIZE, Math.floor(idx / SIZE)]
}

export function sumItems<T extends number[]> (...args: T[]): T {
  return args.reduce((acc, curr) => (
    acc.map((v, i) => v + curr[i]) as T
  ))
}
