
import { State } from './types'
import { SIZE } from './constants'
import { idx } from './arrayHelpers'

let state: State = {
  timestep: 0.1,
  force: 5,
  diffusion: 0,
  viscosity: 0,
  dyeStrength: 10,
  diffuseIterations: 4,
  projectIterations: 4,
  velocities: [
    Array.from(new Array(SIZE * SIZE)).map(() => 0),
    Array.from(new Array(SIZE * SIZE)).map(() => 0),
  ],
  velocityZeroes: [
    Array.from(new Array(SIZE * SIZE)).map(() => 0),
    Array.from(new Array(SIZE * SIZE)).map(() => 0),
  ],
  s: Array.from(new Array(SIZE * SIZE)).map(() => 0),
  dye: Array.from(new Array(SIZE * SIZE)).map(() => 0),
  dyeColor: [255, 128, 255],
}

export const store = {
  setState: (part: Partial<State>) => {
    state = {
      ...state,
      ...part,
    }
  },
  getState: () => state,
  addDye: (x: number, y: number, amount: number) => {
    const i = idx(x, y)
    const dye = [
      ...state.dye.slice(0, i),
      state.dye[i] + amount,
      ...state.dye.slice(i + 1)
    ]
    store.setState({
      dye,
    })
  },
  addVelocity: (x: number, y: number, dx: number, dy: number) => {
    const i = idx(x, y)
    const vx = [
      ...state.velocities[0].slice(0, i),
      state.velocities[0][i] + dx,
      ...state.velocities[0].slice(i + 1)
    ]
    const vy = [
      ...state.velocities[1].slice(0, i),
      state.velocities[1][i] + dy,
      ...state.velocities[1].slice(i + 1)
    ]
    store.setState({
      velocities: [vx,vy]
    })
  }
}
