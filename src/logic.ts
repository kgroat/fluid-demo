
import { idx } from './arrayHelpers'
import { SIZE } from './constants'
import { store } from './store'

export function tick (dt: number) {
  dt = Math.min(dt, 0.2)

  const {
    timestep,
    diffusion,
    diffuseIterations,
    projectIterations,
    viscosity,
    velocities,
    velocityZeroes,
    dye,
    s,
  } = store.getState()

  dt *= timestep

  diffuse(1, velocityZeroes[0], velocities[0], viscosity, diffuseIterations, dt)
  diffuse(2, velocityZeroes[1], velocities[1], viscosity, diffuseIterations, dt)

  project(velocityZeroes, velocities, projectIterations)

  advect(1, velocities[0], velocityZeroes[0], velocityZeroes, dt)
  advect(2, velocities[1], velocityZeroes[1], velocityZeroes, dt)

  project(velocities, velocityZeroes, projectIterations)

  diffuse(0, s, dye, diffusion, dt, diffuseIterations)
  advect(0, dye, s, velocities, dt)
}

export function diffuse (b: number, curr: number[], prev: ReadonlyArray<number>, diff: number, iter: number, timestep: number) {
  const a = timestep * diff * (SIZE - 2) * (SIZE - 2)
  return lin_solve(b, curr, prev, a, 1 + (6 * a), iter)
}

export function project(collectors: [number[], number[]], [p, div]: [number[], number[]], iter: number)
{
  for (let j = 1; j < SIZE - 1; j++) {
    for (let i = 1; i < SIZE - 1; i++) {
      div[idx(i, j)] = -0.5*(
               collectors[0][idx(i+1, j  )]
              -collectors[0][idx(i-1, j  )]
              +collectors[1][idx(i  , j+1)]
              -collectors[1][idx(i  , j-1)]
          ) / SIZE
      p[idx(i, j)] = 0
    }
  }

  set_bnd(0, div)
  set_bnd(0, p)
  lin_solve(0, p, div, 1, 6, iter)

  for (let j = 1; j < SIZE - 1; j++) {
    for (let i = 1; i < SIZE - 1; i++) {
      collectors[0][idx(i, j)] -= 0.5 * (p[idx(i+1, j)] - p[idx(i-1, j)]) * SIZE
      collectors[1][idx(i, j)] -= 0.5 * (p[idx(i, j+1)] - p[idx(i, j-1)]) * SIZE
    }
  }

  set_bnd(1, collectors[0])
  set_bnd(2, collectors[1])
}

export function advect(b: number, curr: number[], prev: ReadonlyArray<number>, velocities: Readonly<[ReadonlyArray<number>, ReadonlyArray<number>]>, dt: number) {
    const dts = dt * (SIZE - 2)

    for(let j = 1; j < SIZE - 1; j++) {
        for(let i = 1; i < SIZE - 1; i++) {
          const index = idx(i, j)
            const x = Math.max(0.5, Math.min(i - dts * velocities[0][index], SIZE + 0.5))
            const y = Math.max(0.5, Math.min(j - dts * velocities[1][index], SIZE + 0.5))
            const xI = Math.floor(x)
            const yI = Math.floor(y)

            const s1 = x - xI
            const s0 = 1.0 - s1
            const t1 = y - yI
            const t0 = 1.0 - t1

            curr[index] =
                s0  * (t0 * prev[idx(xI    , yI    )] + t1 * prev[idx(xI    , yI + 1)])
              + s1  * (t0 * prev[idx(xI + 1, yI    )] + (t1 * prev[idx(xI + 1, yI + 1)]))
        }
    }
    set_bnd(b, curr)
}

function lin_solve (b: number, curr: number[], prev: ReadonlyArray<number>, a: number, c: number, iter: number) {
  const cRecip = 1 / c
  for (let k = 0; k < iter; k++) {
    // const orig = Array.from(curr)
    for (let j = 1; j < SIZE - 1; j++) {
      for (let i = 1; i < SIZE - 1; i++) {
        curr[idx(i, j)] =
            (prev[idx(i, j)]
                + a * (curr[idx(i+1, j  )]
                      +curr[idx(i-1, j  )]
                      +curr[idx(i  , j+1)]
                      +curr[idx(i  , j-1)]
            )) * cRecip
      }
    }

    set_bnd(b, curr)
  }
}

function set_bnd (b: number, x: number[]) {
  for(let i = 1; i < SIZE - 1; i++) {
    x[idx(i, 0  )] = b == 2 ? -x[idx(i, 1  )] : x[idx(i, 1  )]
    x[idx(i, SIZE-1)] = b == 2 ? -x[idx(i, SIZE-2)] : x[idx(i, SIZE-2)]

    x[idx(0  , i)] = b == 1 ? -x[idx(1  , i)] : x[idx(1  , i)]
    x[idx(SIZE-1, i)] = b == 1 ? -x[idx(SIZE-2, i)] : x[idx(SIZE-2, i)]
  }

  x[idx(0, 0)]             = (x[idx(1, 0)] + x[idx(0, 1)]) / 2
  x[idx(0, SIZE-1)]        = (x[idx(1, SIZE-1)] + x[idx(0, SIZE-2)]) / 2
  x[idx(SIZE-1, 0)]        = (x[idx(1, 0)] + x[idx(0, 1)]) / 2
  x[idx(SIZE-1, SIZE-1)]   = (x[idx(1, SIZE-1)] + x[idx(0, SIZE-2)]) / 2
}
