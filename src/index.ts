
import { canvas, render } from './canvas'
import { controls } from './controls'
import { IS_MOBILE, SCALE } from './constants'
import { tick } from './logic'
import { mouseDown, mouseX, mouseY } from './mouse'
import { store } from './store'

let prevTime = performance.now()
let nextFrame = () => {
  const { dyeStrength } = store.getState()
  if (mouseDown) {
    store.addDye(Math.floor(mouseX / SCALE), Math.floor(mouseY / SCALE), dyeStrength)
  }
  const newTime = performance.now()
  tick((newTime - prevTime) / 1000)
  render()
  requestAnimationFrame(nextFrame)
  prevTime = newTime
}

requestAnimationFrame(nextFrame)

const header = document.createElement('h1')
header.appendChild(document.createTextNode(`${IS_MOBILE ? 'Swipe' : 'Click & drag'} around the black box`))

document.body.appendChild(header)
document.body.appendChild(canvas)
document.body.appendChild(controls)

if (IS_MOBILE) {
  const mobileStyle = document.createElement('link')
  mobileStyle.href = '/mobile.css'
  mobileStyle.rel = 'stylesheet'
  mobileStyle.type = 'text/css'
  document.head.appendChild(mobileStyle)
}
